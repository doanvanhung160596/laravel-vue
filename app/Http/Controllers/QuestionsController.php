<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddQuestionRequest;
use App\Question;
use Illuminate\Http\Request;
use function React\Promise\resolve;

class QuestionsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth', ['accept'=>['index','show']]);
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$questions = Question::latest()->with('user')->paginate(5);
        return view('questions.index',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $question = new Question();
        return view('questions.create',compact('question'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddQuestionRequest $request)
    {
        $request->user()->questions()->create($request->only('title','body'));
        return redirect()->route('questions.index')->with('success','Added Question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
    	 $question->increment('views');
    	 return view('questions.show',compact('question'));
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Question $question
	 * @return \Illuminate\Http\Response
	 */
    public function edit(Question $question)
    {
//        $question = Question::findOrFail($id);
		$this->authorize('update', $question);
        return view('questions.edit',compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddQuestionRequest $request,Question $question)
    {
		$this->authorize('update', $question);
		$question->update($request->only('title','body'));
		return redirect()->route('questions.index')->with('success','Updated Question');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Question $question
	 * @return \Illuminate\Http\Response
	 */
    public function destroy(Question $question)
    {
		$this->authorize('delete', $question);
		$question->delete();
        return redirect('/questions')->with('success','Your question has been deleted');
    }
}
