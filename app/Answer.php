<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{

	/**
	 * @return mixed
	 */
	public function question(){
		return $this->belongsTo(Question::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function getBodyHtmlAttribute(){
		return \Parsedown::instance()->text($this->body);
	}

	public static function boot(){
		parent::boot();
		static::created(function ($answer){
			$answer->question->increment('answers_count');
			$answer->question->save();
		});
//		static::saved(function ($answer){
//			echo "Answer saved\n";
//		});
	}

	public function getCreatedDateAttribute(){
		return $this->created_at->diffForHumans();
//		return date_format($this->created_at,"Y/m/d H:i:s");
	}
}
