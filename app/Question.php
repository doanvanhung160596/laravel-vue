<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Question extends Model
{
	protected $fillable = ['title','body'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user(){
		return $this->belongsTo(User::class);
	}

	public function setTitleAttribute($value) {
		$this->attributes['title'] = $value;
		$this->attributes['slug'] = str_slug($value);
	}

	public function getUrlAttribute(){
		return route('questions.show', $this->slug);
	}

	public function getCreatedDateAttribute(){
		return $this->created_at->diffForHumans();
//		return date_format($this->created_at,"Y/m/d H:i:s");
	}

	public function getStatusAttribute(){
		if ($this->answers_count > 0){
			if($this->best_answer_id){
				return "answered-accepted";
			}else{
				return "answered";
			}
		}
		return "unanswered";
	}

	public function getBodyHtmlAttribute(){
		return \Parsedown::instance()->text($this->body);
	}

	public function answers(){
		return $this->hasMany(Answer::class);
	}
}
